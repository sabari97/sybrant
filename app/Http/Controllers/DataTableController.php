<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Validator,Redirect,Response;
Use App\Todo;
 
class DataTableTodoCrudController extends Controller
{
   /* Display todo list */

    public function index() 
    {
        if(request()->ajax()) {
            return datatables()->of(Todo::select([
                'id','title' , 'description', 'created_at'
            ]))
            ->addIndexColumn()
            ->addColumn('action', function($data){
                   
                   $btn = '<a href="javascript:void(0)" data-toggle="tooltip" data-original-title="Edit" class="edit btn btn-primary btn-sm" onclick=editTodo("'.$data->id.'")>Edit</a>';

                   $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-original-title="Delete" class="btn btn-danger btn-sm " onclick=editTodo("'.$data->id.'")>Delete</a>';

                    return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
        }
        return view('list-todo');
    }
    
    
    /* insert and update todo into mysql database*/

    public function store(Request $request)
    {
        $data = request()->validate([
        'title' => 'required',
        'description' => 'required',
        ]);
       
        if($request->todo_id != '') {

          $check = Todo::where('id', $request->todo_id)->update($data);

        }else{
          
          $check = Todo::create($data);

        }
        
        return Response::json($check);

    }

    /* display edit todo form with data */

    public function edit(Request $request, $id)
    {
       
        $data['todo'] = Todo::where('id', $id)->first();

        return Response::json($data['todo']);
    }
    
    /* delete todo from mysql database */

    public function delete(Request $request, $id)
    {
        $check = Todo::where('id', $id)->delete();
 
        return Response::json($check);
    }

}