<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//display todo list
Route::get('list','DataTableController@index');

//insert and update todo into database
Route::post('store','DataTableController@store');

//display edit todo model
Route::get('edit/{id?}','DataTableController@edit');

//delete todo from mysql database
Route::get('delete/{id?}','DataTableController@delete');