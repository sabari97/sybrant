<!DOCTYPE html>
 
<html lang="en">
<head>

<title>Laravel DataTables Todo CRUD Example Tutorial - w3alert.com</title>

<meta name="csrf-token" content="{{ csrf_token() }}">

<script src="https://code.jquery.com/jquery-3.4.1.js"></script> 

<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">

<link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

</head>
    <body>
         <div class="container mt-4">
            <h2 class="text-center mt-2 mb-3 alert alert-success">Laravel Ajax Todo CRUD with DataTables Example Tutorial</h2>
            <a href="javascript:void(0)" class="text-center btn btn-success mb-1" onclick="addTodo()">Create Todo</a>
            <table class="table table-bordered" id="laravel-datatable-ajax-crud">
               <thead>
                  <tr>
                     <th>Id</th>
                     <th>Title</th>
                     <th>Description</th>
                     <th>Created at</th>
                     <th>Action</th>
                  </tr>
               </thead>
            </table>
         </div>

<div class="modal fade" id="todo-modal" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">

          <div class="modal-header">
              <h4 class="modal-title" id="todo-modal"></h4>
          </div>

          <div class="modal-body">

              <form id="todoForm" name="todoForm" class="form-horizontal">

                 <input type="hidden" name="todo_id" id="todo_id">

                  <div class="form-group">
                      <label for="name" class="col-sm-2 control-label">Title</label>
                      <div class="col-sm-12">
                          <input type="text" class="form-control" id="title" name="title" placeholder="Enter Tilte" value="" maxlength="50" required="">
                      </div>
                  </div> 
       
                  <div class="form-group">
                      <label class="col-sm-2 control-label">Description</label>
                      <div class="col-sm-12">
                          <input type="text" class="form-control" id="description" name="description" placeholder="Enter Description" value="" required="">
                      </div>
                  </div>

                  <div class="col-sm-offset-2 col-sm-10">
                   <button type="submit" class="btn btn-primary" id="btn-save" value="create">Save changes
                   </button>
                  </div>

              </form>

          </div>
          <div class="modal-footer">
              
          </div>
      </div>
  </div>
</div>
<script>

   $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

  $(document).ready( function () {

      $('#laravel-datatable-ajax-crud').DataTable({
           processing: true,
           serverSide: true,
           ajax: {
            url: "{{ url('list') }}",
            type: 'GET',
           },
           columns: [
                    { data: 'id', name: 'id' },
                    { data: 'title', name: 'title' },
                    { data: 'description', name: 'description' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'action', name: 'action' }
                 ]
       });
   });
    

     //for add todo
   function addTodo()
    {   
        $('#todo-modal').find("input,textarea").val('').end();
        $('#todo-modal').modal('show');
    }
    
    // for edit todo
   function editTodo(id)
    { 
      $('#todo-modal').find("input,textarea").val('').end();
      $.get('edit'+'/'+id, function (data) {
          $('#todo-modal').modal('show');
          $('#todo_id').val(data.id);
          $('#title').val(data.title);
          $('#description').val(data.description);
      })
    }
    
    //for delete todo
    function deleteTodo(id)
     {
        if(confirm("Are You really want to delete this todo!")){

        $.ajax({
            type: "get",
            url: "{{ url('delete') }}"+'/'+id,
            success: function (data) {
            var oTable = $('#laravel-datatable-ajax-crud').dataTable(); 
            oTable.fnDraw(false);
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
       }
     }
 
    
    if ($("#todoForm").length > 0) {
        $("#todoForm").validate({

         submitHandler: function(form) {

         var actionType = $('#btn-save').val();
         $('#btn-save').html('Sending..');
       
         $.ajax({
            data: $('#todoForm').serialize(),
            url: "{{ url('store') }}",
            type: "POST",
            dataType: 'json',
            success: function (data) {

                $('#todoForm').trigger("reset");
                $('#todo-modal').modal('hide');
                $('#btn-save').html('Save Changes');
                var oTable = $('#laravel-datatable-ajax-crud').dataTable();
                oTable.fnDraw(false);
               
            },
            error: function (data) {
                console.log('Error:', data);
                $('#btn-save').html('Save Changes');
            }
         });
        }
      })
    }  

</script>
</body>
</html>